/**
 * COMP 4300 - Assignment 4
 *
 * Created by:
 *      - Devin Marsh - 201239464
 *      - Justin Delaney - 201222684
 *
 * Press Enter to shoot a flying sword!
 */

#include <SFML/Graphics.hpp>
#include "GameEngine.h"

int main()
{
    GameEngine g("assets.txt");
    g.run();
}