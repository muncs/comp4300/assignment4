#include "GameState_Play.h"
#include "Common.h"
#include "Physics.h"
#include "Assets.h"
#include "GameEngine.h"
#include "Components.h"

GameState_Play::GameState_Play(GameEngine & game, const std::string & levelPath)
    : GameState(game)
    , m_levelPath(levelPath)
{
    init(m_levelPath);
}

void GameState_Play::init(const std::string & levelPath)
{
    loadLevel(levelPath);
}

void GameState_Play::loadLevel(const std::string & filename)
{
    m_entityManager = EntityManager();

	//Read in level config file
	std::ifstream fin(m_levelPath);
	std::string token;

	while (fin.good())
	{
		fin >> token;
		
		if (token == "Player")
		{
			int posX, posY;
			int bX, bY;
			float player_speed;

			fin >> posX >> posY >> bX >> bY >> player_speed;

			m_playerConfig.X = posX;
			m_playerConfig.Y = posY;
			m_playerConfig.CX = bX;
			m_playerConfig.CY = bY;
			m_playerConfig.SPEED = player_speed;
		}
		else if (token == "Tile")
		{
			std::string name;
			int rX, rY;
			int posX, posY;
			int bM, bV;

			fin >> name >> rX >> rY >> posX >> posY >> bM >> bV;

			// Calculate where to add tile entities
			Vec2 tilePos = Vec2(rX*1280, rY*768) + Vec2(posX*64 + 32, posY*64 + 32);

			auto tiles = m_entityManager.addEntity("tile");

			tiles->addComponent<CTransform>(tilePos);
			tiles->addComponent<CAnimation>(m_game.getAssets().getAnimation(name), true);
			tiles->addComponent<CBoundingBox>(m_game.getAssets().getAnimation(name).getSize(), bM, bV);

		}
		else if (token == "NPC")
		{
			std::string name;
			int rX, rY;
			int posX, posY;
			int bM, bV;
			std::string AI_type;
			int Speed;

			fin >> name >> rX >> rY >> posX >> posY >> bM >> bV >> AI_type >> Speed;

			// Calculate where to add NPCs
			Vec2 NPC_Pos = Vec2(rX * 1280, rY * 768) + Vec2(posX * 64 + 32, posY * 64 + 32);

			auto enemies = m_entityManager.addEntity("npc");

			enemies->addComponent<CTransform>(NPC_Pos);
			enemies->addComponent<CAnimation>(m_game.getAssets().getAnimation(name), true);
			enemies->addComponent<CBoundingBox>(m_game.getAssets().getAnimation(name).getSize(), bM, bV);

			//AI behavour
			if (AI_type == "Follow")
			{
				enemies->addComponent<CFollowPlayer>(NPC_Pos, Speed);
			}
			else if(AI_type == "Patrol")
			{
				int numPatrolPoints;
				int X, Y;
				
				fin >> numPatrolPoints;

				//Coords for each patrol point
				std::vector<Vec2> patrolPoints;

				//Add each patrol point to the correct location 
				for (int i = 0; i < numPatrolPoints; i++)
				{
					int x, y;
					fin >> x >> y;
					patrolPoints.push_back(Vec2(rX * 1280, rY * 768) + Vec2(x * 64 + 32, y * 64 + 32));
				}

				enemies->addComponent<CPatrol>(patrolPoints, Speed);
			}
		}
	}

    //Spawn player
    spawnPlayer();
}

void GameState_Play::spawnPlayer()
{
    m_player = m_entityManager.addEntity("player");
    m_player->addComponent<CTransform>(Vec2(m_playerConfig.X, m_playerConfig.Y));
    m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("StandDown"), true);
    m_player->addComponent<CInput>();
	m_player->addComponent<CBoundingBox>(Vec2(m_playerConfig.CX, m_playerConfig.CY), true, true);
	m_player->addComponent<CState>("Standing");
    
    // New element to CTransform: 'facing', to keep track of where the player is facing
    m_player->getComponent<CTransform>()->facing = Vec2(0, 1);
}

void GameState_Play::spawnSword(std::shared_ptr<Entity> entity)
{
    auto eTransform = entity->getComponent<CTransform>();

    auto sword = m_entityManager.addEntity ("sword");
    sword->addComponent<CTransform>(entity->getComponent<CTransform>()->pos + eTransform->facing * 64);

	//Horizontal
	if (eTransform->facing.x != 0)
	{
		sword->addComponent<CAnimation>(m_game.getAssets().getAnimation("SwordRight"), true);

		//Invert animation on X axis
		if (eTransform->facing.x == -1)
		{
			sword->getComponent<CTransform>()->scale.x *= -1;
		}
	}
	//Vertical
	else
	{
		sword->addComponent<CAnimation>(m_game.getAssets().getAnimation("SwordUp"), true);

		//Invert animation on Y axis
		if (eTransform->facing.y == 1)
		{
			sword->getComponent<CTransform>()->scale.y *= -1;
		}
	}

	sword->addComponent<CBoundingBox>(sword->getComponent<CAnimation>()->animation.getSize(), false, false);
	sword->addComponent<CLifeSpan>(150);
}

const int SPECIAL_DURATION = 2000;
const int SPECIAL_SPEED = 10;
void GameState_Play::spawnSpecial(std::shared_ptr<Entity> entity)
{
	auto eTransform = entity->getComponent<CTransform>();

	auto special = m_entityManager.addEntity("special");
	special->addComponent<CTransform>(entity->getComponent<CTransform>()->pos + eTransform->facing * 64);

	//Horizontal
	if (eTransform->facing.x != 0)
	{
		special->addComponent<CAnimation>(m_game.getAssets().getAnimation("SwordRight"), true);
		special->getComponent<CTransform>()->speed = Vec2(SPECIAL_SPEED, 0);

		//Invert animation on X axis
		if (eTransform->facing.x == -1)
		{
			special->getComponent<CTransform>()->scale.x *= -1;
			special->getComponent<CTransform>()->speed = Vec2(-SPECIAL_SPEED, 0);
		}
	}
	//Vertical
	else
	{
		special->addComponent<CAnimation>(m_game.getAssets().getAnimation("SwordUp"), true);
		special->getComponent<CTransform>()->speed = Vec2(0, -SPECIAL_SPEED);

		//Invert animation on Y axis
		if (eTransform->facing.y == 1)
		{
			special->getComponent<CTransform>()->scale.y *= -1;
			special->getComponent<CTransform>()->speed = Vec2(0, SPECIAL_SPEED);
		}
	}

	special->addComponent<CBoundingBox>(special->getComponent<CAnimation>()->animation.getSize(), false, false);
	special->addComponent<CLifeSpan>(SPECIAL_DURATION);
}

void GameState_Play::update()
{
    m_entityManager.update();

    if (!m_paused)
    {
        sAI();
        sMovement();
        sLifespan();
        sCollision();
        sAnimation();
    }

    sUserInput();
    sRender();
}

void GameState_Play::sMovement()
{
	//Reset speed every frame
	m_player->getComponent<CTransform>()->speed.x = 0;
	m_player->getComponent<CTransform>()->speed.y = 0;

	if (m_player->hasComponent<CTransform>())
	{
		//Vertical movement
		if (m_player->getComponent<CInput>()->up && !m_player->getComponent<CInput>()->down)
		{
			m_player->getComponent<CTransform>()->speed.x = 0;
			m_player->getComponent<CState>()->state = "RunUp";
			m_player->getComponent<CTransform>()->facing = Vec2(0, -1);
			m_player->getComponent<CTransform>()->speed.y -= m_playerConfig.SPEED; 
		}
		else if (m_player->getComponent<CInput>()->down && !m_player->getComponent<CInput>()->up)
		{
			m_player->getComponent<CTransform>()->speed.x = 0;
			m_player->getComponent<CState>()->state = "RunDown";
			m_player->getComponent<CTransform>()->facing = Vec2(0, 1);
			m_player->getComponent<CTransform>()->speed.y += m_playerConfig.SPEED; 
		}

		//Horizontal movement
		if (m_player->getComponent<CInput>()->left && !m_player->getComponent<CInput>()->right)
		{
			m_player->getComponent<CTransform>()->speed.y = 0;
			m_player->getComponent<CState>()->state = "RunLeft";
			m_player->getComponent<CTransform>()->facing = Vec2(-1, 0);
			m_player->getComponent<CTransform>()->speed.x -= m_playerConfig.SPEED; 
		}
		else if (m_player->getComponent<CInput>()->right && !m_player->getComponent<CInput>()->left)
		{
			m_player->getComponent<CTransform>()->speed.y = 0;
			m_player->getComponent<CState>()->state = "RunRight";
			m_player->getComponent<CTransform>()->facing = Vec2(1, 0);
			m_player->getComponent<CTransform>()->speed.x += m_playerConfig.SPEED; 
		}

		//No movement (Standing)
		if (!m_player->getComponent<CInput>()->up && !m_player->getComponent<CInput>()->down && !m_player->getComponent<CInput>()->left && !m_player->getComponent<CInput>()->right)
		{
			//Vertical
			if (m_player->getComponent<CTransform>()->facing.x == 0 && m_player->getComponent<CTransform>()->facing.y == -1)
			{
				m_player->getComponent<CTransform>()->speed.x = 0;
				m_player->getComponent<CState>()->state = "StandUp";
			}
			else if (m_player->getComponent<CTransform>()->facing.x == 0 && m_player->getComponent<CTransform>()->facing.y == 1)
			{
				m_player->getComponent<CTransform>()->speed.x = 0;
				m_player->getComponent<CState>()->state = "StandDown";
			}
			//Horizontal
			else if (m_player->getComponent<CTransform>()->facing.y == 0 && m_player->getComponent<CTransform>()->facing.x == -1)
			{
				m_player->getComponent<CTransform>()->speed.y = 0;
				m_player->getComponent<CState>()->state = "StandLeft";
			}
			else if (m_player->getComponent<CTransform>()->facing.y == 0 && m_player->getComponent<CTransform>()->facing.x == 1)
			{
				m_player->getComponent<CTransform>()->speed.y = 0;
				m_player->getComponent<CState>()->state = "StandRight";
			}
		}

		//Limit player speed
		m_player->getComponent<CTransform>()->speed.x = fmin(m_playerConfig.SPEED, fmax(m_player->getComponent<CTransform>()->speed.x, -m_playerConfig.SPEED));
		m_player->getComponent<CTransform>()->speed.y = fmin(m_playerConfig.SPEED, fmax(m_player->getComponent<CTransform>()->speed.y, -m_playerConfig.SPEED));
	}

	//All other entities
	for (auto entity : m_entityManager.getEntities())
	{
		if (entity->hasComponent<CTransform>())
		{
			entity->getComponent<CTransform>()->prevPos = entity->getComponent<CTransform>()->pos;
			entity->getComponent<CTransform>()->pos += entity->getComponent<CTransform>()->speed;

			//Update the sword's position as it follows with the player
			//Updates for the next frame
			if (entity->tag() == "sword")
			{
				m_player->getComponent<CInput>()->shoot = false;
				entity->getComponent<CTransform>()->facing = m_player->getComponent<CTransform>()->facing;
				entity->getComponent<CTransform>()->pos = m_player->getComponent<CTransform>()->pos + m_player->getComponent<CTransform>()->facing * 64;
				m_player->getComponent<CState>()->state = "Attack";
			}
		}
	}
}

const int PATROL_DISTANCE_MARGIN = 5;
void GameState_Play::sAI()
{
	for (auto entity : m_entityManager.getEntities("npc"))
	{
		Vec2 destination(0, 0);
		float speed;
		
		entity->getComponent<CTransform>()->speed = Vec2(0, 0);

		//Patrol NPCs
		if (entity->hasComponent<CPatrol>())
		{
			destination = entity->getComponent<CPatrol>()->positions[entity->getComponent<CPatrol>()->currentPosition];
			speed = entity->getComponent<CPatrol>()->speed;

			//If enemy is within a margin of PATROL DISTANCE, move to player
			if (entity->getComponent<CTransform>()->pos.dist(destination) <= PATROL_DISTANCE_MARGIN)
			{
				entity->getComponent<CPatrol>()->currentPosition += 1;

				if (entity->getComponent<CPatrol>()->positions.size() <= entity->getComponent<CPatrol>()->currentPosition)
				{
					entity->getComponent<CPatrol>()->currentPosition = 0;
				}

				destination = entity->getComponent<CPatrol>()->positions[entity->getComponent<CPatrol>()->currentPosition];
			}
		}
		//Follow NPCs
		else if (entity->hasComponent<CFollowPlayer>() && entity->hasComponent<CBoundingBox>())
		{
			bool inSight = true;
			speed = entity->getComponent<CFollowPlayer>()->speed;

			for (auto e : m_entityManager.getEntities())
			{
				//Skip player and itself
				if (e->tag() == "player" || entity == e) continue;

				if (e->hasComponent<CBoundingBox>() && e->getComponent<CBoundingBox>()->blockVision)
				{
					//If vision is blocked then cancel movement towards player
					if (Physics::EntityIntersect(m_player->getComponent<CTransform>()->pos, entity->getComponent<CTransform>()->pos, e) || entity->getComponent<CTransform>()->pos.dist(m_player->getComponent<CTransform>()->pos) > 10 * 64) // Check if sight is blocked by entity OR out of range
					{
						inSight = false;
						break;
					}
				}
			}

			//Move towards player if in sight
			if (inSight == true)
			{
				destination = m_player->getComponent<CTransform>()->pos;
			}
			else
			{
				destination = entity->getComponent<CFollowPlayer>()->home;
			}		
		}

		//Handle NPC speed
		if (entity->getComponent<CTransform>()->pos.dist(destination) <= PATROL_DISTANCE_MARGIN)
		{
			entity->getComponent<CTransform>()->speed = Vec2(0, 0);
		}
		else
		{
			float deltaX, deltaY, tanAngle;
			
			deltaX = destination.x - entity->getComponent<CTransform>()->pos.x;
			deltaY = destination.y - entity->getComponent<CTransform>()->pos.y;
			tanAngle = atan2(deltaY, deltaX);

			entity->getComponent<CTransform>()->speed = Vec2(speed * cos(tanAngle), speed * sin(tanAngle));
		}
	}
}

void GameState_Play::sLifespan()
{
	for (auto & entity : m_entityManager.getEntities()) {
		if (entity->hasComponent<CLifeSpan>() && !m_paused)
		{
			const float ratio = entity->getComponent<CLifeSpan>()->clock.getElapsedTime().asMilliseconds() / (float)entity->getComponent<CLifeSpan>()->lifespan;

			if (ratio >= 1.0)
			{
				entity->destroy();
			}
		}
	}
}

void GameState_Play::sCollision()
{
	for (auto & tile : m_entityManager.getEntities("tile")) {
		Vec2 overlap = Physics::GetOverlap(m_player, tile);
		Vec2 prevOverlap = Physics::GetPreviousOverlap(m_player, tile);

		const auto tileType = tile->getComponent<CAnimation>()->animation.getName();
		const auto tileTransform = tile->getComponent<CTransform>();
		const auto playerTransform = m_player->getComponent<CTransform>();
		auto playerState = m_player->getComponent<CState>();

		//Collide against all tiles besides 'black'
		if (tileType != "Black")
		{
			//Collision happens if theres an overlap in both axis
			if (overlap.x > 0 && overlap.y > 0) {
				//Player vs tile top
				if (prevOverlap.x > 0 && tileTransform->prevPos.y < playerTransform->prevPos.y)
				{
					playerTransform->pos.y += overlap.y;
					playerTransform->speed.y = 0;

					if (playerTransform->speed.x == 0)
					{
						playerState->state = "standing";
					}
					else
					{
						playerState->state = "running";
					}
				}
				//Player vs tile bottom
				else if (prevOverlap.x > 0 && tileTransform->prevPos.y > playerTransform->prevPos.y)
				{
					playerTransform->pos.y -= overlap.y;
					playerTransform->speed.y = 0;
				}

				//Player vs tile left
				else if (prevOverlap.y > 0 && tileTransform->prevPos.x < playerTransform->prevPos.x)
				{
					playerTransform->pos.x += overlap.x;
				}

				//Player vs tile right
				else if (prevOverlap.y > 0 && tileTransform->prevPos.x > playerTransform->prevPos.x)
				{
					playerTransform->pos.x -= overlap.x;
				}
			}
		}

		//Special vs tile
		for (auto & special : m_entityManager.getEntities("special"))
		{
			Vec2 overlap = Physics::GetOverlap(special, tile);

			if (overlap.x > 0 && overlap.y > 0)
			{
				//Special dies if hits tile
				special->destroy();
			}
		}
	}

	//Player vs enemy NPCs
	for (auto & npc : m_entityManager.getEntities("npc"))
	{
		Vec2 overlap = Physics::GetOverlap(m_player, npc);

		// Player dies if they collide with enemy NPC
		if (overlap.x > 0 && overlap.y > 0)
		{
			m_player->destroy();
			spawnPlayer();
		}

		//NPC vs sword
		for (auto & sword : m_entityManager.getEntities("sword"))
		{
			Vec2 overlap = Physics::GetOverlap(sword, npc);

			if (overlap.x > 0 && overlap.y > 0)
			{
				if (npc->getComponent<CAnimation>()->animation.getName() != "Explosion")
				{
					auto boom = m_entityManager.addEntity("dec");

					boom->addComponent<CTransform>()->pos = npc->getComponent<CTransform>()->pos;
					boom->addComponent<CAnimation>(m_game.getAssets().getAnimation("Explosion"), false);
				}

				// NPC dies if hit with sword
				npc->destroy();
			}
		}

		for (auto & special : m_entityManager.getEntities("special"))
		{
			Vec2 overlap = Physics::GetOverlap(special, npc);

			if (overlap.x > 0 && overlap.y > 0)
			{
				if (npc->getComponent<CAnimation>()->animation.getName() != "Explosion")
				{
					auto boom = m_entityManager.addEntity("dec");

					boom->addComponent<CTransform>()->pos = npc->getComponent<CTransform>()->pos;
					boom->addComponent<CAnimation>(m_game.getAssets().getAnimation("Explosion"), false);
				}

				// NPC dies if hit with sword
				npc->destroy();
				special->destroy();
			}
		}

		//NPC vs tiles
		for (auto & tile : m_entityManager.getEntities("tile"))
		{
			const auto npcTransform = npc->getComponent<CTransform>();
			Vec2 overlap = Physics::GetOverlap(npc, tile);
			Vec2 prevOverlap = Physics::GetPreviousOverlap(npc, tile);
			const auto tileTransform = tile->getComponent<CTransform>();

			if (overlap.x > 0 & overlap.y > 0)
			{
				if (overlap.x > 0 && overlap.y > 0) {
					//NPC vs tile top
					if (prevOverlap.x > 0 && tileTransform->prevPos.y < npcTransform->prevPos.y)
					{
						npcTransform->pos.y += overlap.y;
						npcTransform->speed.y = 0;
					}
					//NPC vs tile bottom
					else if (prevOverlap.x > 0 && tileTransform->prevPos.y > npcTransform->prevPos.y)
					{
						npcTransform->pos.y -= overlap.y;
						npcTransform->speed.y = 0;
					}

					//Player vs tile left
					else if (prevOverlap.y > 0 && tileTransform->prevPos.x < npcTransform->prevPos.x)
					{
						npcTransform->pos.x += overlap.x;
					}

					//NPC vs tile right
					else if (prevOverlap.y > 0 && tileTransform->prevPos.x > npcTransform->prevPos.x)
					{
						npcTransform->pos.x -= overlap.x;
					}
				}
			}
		}
	}
}

void GameState_Play::sAnimation()
{
	for (auto entity : m_entityManager.getEntities()) 
	{
		//Skip entity if it doesn't have an animation
		if (!entity->hasComponent<CAnimation>()) { continue; }
		auto animation = entity->getComponent<CAnimation>();

		//Handle player animation
		if (entity == m_player)
		{
			//Vertical running
			if (m_player->getComponent<CState>()->state == "RunUp" && animation->animation.getName() != "RunUp")
			{
				m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("RunUp"), true);
			}
			else if (m_player->getComponent<CState>()->state == "RunDown" && animation->animation.getName() != "RunDown")
			{
				m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("RunDown"), true);
			}
			//Horizontal running
			else if (m_player->getComponent<CState>()->state == "RunLeft" || m_player->getComponent<CState>()->state == "RunRight")
			{
				if (animation->animation.getName() != "RunRight")
				{
					m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("RunRight"), true);
				}
				m_player->getComponent<CTransform>()->scale.x = m_player->getComponent<CTransform>()->facing.x;
			}
			//Vertical standing
			else if (m_player->getComponent<CState>()->state == "StandUp" && animation->animation.getName() != "StandUp")
			{
				m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("StandUp"), true);
			}
			else if (m_player->getComponent<CState>()->state == "StandDown" && animation->animation.getName() != "StandDown")
			{
				m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("StandDown"), true);
			}
			//Horizontal standing
			else if (m_player->getComponent<CState>()->state == "StandLeft" || m_player->getComponent<CState>()->state == "StandRight")
			{
				if (animation->animation.getName() != "StandRight")
				{
					m_player->addComponent<CAnimation>(m_game.getAssets().getAnimation("StandRight"), true);
				}
				m_player->getComponent<CTransform>()->scale.x = m_player->getComponent<CTransform>()->facing.x;
			}
		}

		//Attack down
		if (m_player->getComponent<CState>()->state == "Attack" && m_player->getComponent<CTransform>()->facing == Vec2(0, 1))
		{
			auto attack_down = m_game.getAssets().getAnimation("AtkDown");
			m_player->getComponent<CAnimation>()->animation = attack_down;
		}

		//Attack up
		if (m_player->getComponent<CState>()->state == "Attack" && m_player->getComponent<CTransform>()->facing == Vec2(0, -1))
		{
			auto attack_up = m_game.getAssets().getAnimation("AtkUp");
			m_player->getComponent<CAnimation>()->animation = attack_up;
		}

		//Attack right
		if (m_player->getComponent<CState>()->state == "Attack" && m_player->getComponent<CTransform>()->facing == Vec2(-1, 0))
		{
			auto attack_right = m_game.getAssets().getAnimation("AtkRight");
			m_player->getComponent<CAnimation>()->animation = attack_right;
		}

		//Attack left
		if (m_player->getComponent<CState>()->state == "Attack" && m_player->getComponent<CTransform>()->facing == Vec2(1, 0))
		{
			auto attack_right = m_game.getAssets().getAnimation("AtkRight");
			m_player->getComponent<CAnimation>()->animation = attack_right;
			
		}

		//Sword
		if (entity->tag() == "sword")
		{
			entity->getComponent<CTransform>()->scale = Vec2(1, 1);
			auto swordFace = entity->getComponent<CTransform>()->facing.y;

			//Handle facing
			if (abs(swordFace) == 1)
			{
				entity->addComponent<CAnimation>(m_game.getAssets().getAnimation("SwordUp"), true);
				entity->getComponent<CTransform>()->scale.y = entity->getComponent<CTransform>()->facing.y * -1;
			}
			else
			{
				entity->addComponent<CAnimation>(m_game.getAssets().getAnimation("SwordRight"), true);
				entity->getComponent<CTransform>()->scale.x = entity->getComponent<CTransform>()->facing.x;
			}
		}

		//Update
		animation->animation.update();

		//Destroy entity if animation does not repeat and has finish
		if (animation->animation.hasEnded() && !animation->repeat) {
			entity->destroy();
		}
	}
}

void GameState_Play::sUserInput()
{
    auto pInput = m_player->getComponent<CInput>();

    sf::Event event;
    while (m_game.window().pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            m_game.quit();
        }
        // this event is triggered when a key is pressed
        if (event.type == sf::Event::KeyPressed)
        {
            switch (event.key.code)
            {
                case sf::Keyboard::Escape:  { m_game.popState(); break; }
                case sf::Keyboard::W:       { pInput->up = true; break; }
                case sf::Keyboard::A:       { pInput->left = true; break; }
                case sf::Keyboard::S:       { pInput->down = true; break; }
                case sf::Keyboard::D:       { pInput->right = true; break; }
                case sf::Keyboard::Z:       { init(m_levelPath); break; }
                case sf::Keyboard::R:       { m_drawTextures = !m_drawTextures; break; }
                case sf::Keyboard::F:       { m_drawCollision = !m_drawCollision; break; }
                case sf::Keyboard::Y:       { m_follow = !m_follow; break; }
                case sf::Keyboard::P:       { setPaused(!m_paused); break; }
                case sf::Keyboard::Space:   { spawnSword(m_player); break; }
				case sf::Keyboard::Enter:   { spawnSpecial(m_player); break; }
            }
        }

        if (event.type == sf::Event::KeyReleased)
        {
            switch (event.key.code)
            {
                case sf::Keyboard::W:       { pInput->up = false; break; }
                case sf::Keyboard::A:       { pInput->left = false; break; }
                case sf::Keyboard::S:       { pInput->down = false; break; }
                case sf::Keyboard::D:       { pInput->right = false; break; }
                case sf::Keyboard::Space:   { pInput->shoot = false; pInput->canShoot = true; break; }
            }
        }
    }
}

void GameState_Play::sRender()
{
    m_game.window().clear(sf::Color(255, 192, 122));
	sf::View playerView;

	playerView.setSize(1280, 768);

	float playerPos_X = m_player->getComponent<CTransform>()->pos.x;
	float playerPos_Y = m_player->getComponent<CTransform>()->pos.y;

	//Set the camera to follow player's position 
	if (m_follow)
	{
		playerView.setCenter(sf::Vector2f(playerPos_X, playerPos_Y));
	}
	//Set camera to overview the room 
	else if (!m_follow)
	{
		int rx = floorf(m_player->getComponent<CTransform>()->pos.x / 1280) * 1280;
		int ry = floorf(m_player->getComponent<CTransform>()->pos.y / 768) * 768;

		playerView.reset(sf::FloatRect(rx, ry, 1280, 768));
	}

	m_game.window().setView(playerView);
        
    //Draw all Entity textures / animations
    if (m_drawTextures)
    {
        for (auto e : m_entityManager.getEntities())
        {
            auto transform = e->getComponent<CTransform>();

            if (e->hasComponent<CAnimation>())
            {
                auto animation = e->getComponent<CAnimation>()->animation;
                animation.getSprite().setRotation(transform->angle);
                animation.getSprite().setPosition(transform->pos.x, transform->pos.y);
                animation.getSprite().setScale(transform->scale.x, transform->scale.y);
                m_game.window().draw(animation.getSprite());
            }
        }
    }

    //Draw all Entity collision bounding boxes with a rectangleshape
    if (m_drawCollision)
    {
        sf::CircleShape dot(4);
        dot.setFillColor(sf::Color::Black);
        for (auto e : m_entityManager.getEntities())
        {
            if (e->hasComponent<CBoundingBox>())
            {
                auto box = e->getComponent<CBoundingBox>();
                auto transform = e->getComponent<CTransform>();
                sf::RectangleShape rect;
                rect.setSize(sf::Vector2f(box->size.x-1, box->size.y-1));
                rect.setOrigin(sf::Vector2f(box->halfSize.x, box->halfSize.y));
                rect.setPosition(transform->pos.x, transform->pos.y);
                rect.setFillColor(sf::Color(0, 0, 0, 0));

                if (box->blockMove && box->blockVision)  { rect.setOutlineColor(sf::Color::Black); }
                if (box->blockMove && !box->blockVision) { rect.setOutlineColor(sf::Color::Blue); }
                if (!box->blockMove && box->blockVision) { rect.setOutlineColor(sf::Color::Red); }
                if (!box->blockMove && !box->blockVision) { rect.setOutlineColor(sf::Color::White); }
                rect.setOutlineThickness(1);
                m_game.window().draw(rect);
            }

            if (e->hasComponent<CPatrol>())
            {
                auto & patrol = e->getComponent<CPatrol>()->positions;
                for (size_t p = 0; p < patrol.size(); p++)
                {
                    dot.setPosition(patrol[p].x, patrol[p].y);
                    m_game.window().draw(dot);
                }
            }

            if (e->hasComponent<CFollowPlayer>())
            {
                sf::VertexArray lines(sf::LinesStrip, 2);
                lines[0].position.x = e->getComponent<CTransform>()->pos.x;
                lines[0].position.y = e->getComponent<CTransform>()->pos.y;
                lines[0].color = sf::Color::Black;
                lines[1].position.x = m_player->getComponent<CTransform>()->pos.x;
                lines[1].position.y = m_player->getComponent<CTransform>()->pos.y;
                lines[1].color = sf::Color::Black;
                m_game.window().draw(lines);
                dot.setPosition(e->getComponent<CFollowPlayer>()->home.x, e->getComponent<CFollowPlayer>()->home.y);
                m_game.window().draw(dot);
            }
        }
    }

    m_game.window().display();
}